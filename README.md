# inSched Discord Bot

## Status
[![pipeline status](https://gitlab.com/fikri.akmal/testing-insched/badges/master/pipeline.svg)](https://gitlab.com/fikri.akmal/testing-insched/-/commits/master)
[![coverage report](https://gitlab.com/fikri.akmal/testing-insched/badges/master/coverage.svg)](https://gitlab.com/fikri.akmal/testing-insched/-/commits/master)

## Deskripsi
**inSched Discord Bot** InSched (Instant Scheduler) adalah sebuah Discord Bot yang bertujuan memudahkan user untuk mengatur schedule-nya dengan memanfaatkan Google Calendar API.

## Anggota A12
1. 1806205275 - Darian Texanditama [![coverage report](https://gitlab.com/fikri.akmal/testing-insched/badges/feature/booking/coverage.svg)](https://gitlab.com/fikri.akmal/testing-insched/-/commits/feature/booking)
2. 1906285485 - Widyanto Hadi Nugroho [![coverage report](https://gitlab.com/fikri.akmal/testing-insched/badges/dev/widy/coverage.svg)](https://gitlab.com/fikri.akmal/testing-insched/-/commits/dev/widy)
3. 1906293146 - Marcia Nadin Pramasiwi [![coverage report](https://gitlab.com/fikri.akmal/testing-insched/badges/showCalendaaar/coverage.svg)](https://gitlab.com/fikri.akmal/testing-insched/-/commits/showCalendaaar)
4. 1906307132 - Fikri Akmal [![coverage report](https://gitlab.com/fikri.akmal/testing-insched/badges/dev/fikri-new/coverage.svg)](https://gitlab.com/fikri.akmal/testing-insched/-/commits/dev/fikri-new)
5. 1906398774 - Mu'adz [![coverage report](https://gitlab.com/fikri.akmal/testing-insched/badges/dev/fikri-new/coverage.svg)](https://gitlab.com/fikri.akmal/testing-insched/-/commits/CreateEvent-new)

## Pembagian Tugas
1. Authentication/Login & Google API (Widyanto Nugroho)
2. Create event (Mu’adz)
3. Create appointment slot (Fikri Akmal)
4. Booking appointment (Darian Texanditama)
5. Show calendar (Marcia Nadin)
